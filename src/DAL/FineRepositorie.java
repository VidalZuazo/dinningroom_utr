/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTL.Fine;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author AlbertFields
 */
public class FineRepositorie {
        final static String USER ="DESKTOP-5GUJB6U";
        final static int PORT = 1433;
        final static String DB_NAME ="Comedor";
        
        
  private static ArrayList<Fine> getDataByQuery(String query) {
        ArrayList<Fine> oFine = new ArrayList<>();
        
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                Fine F = new Fine();
                F.setFineID(rs.getInt("FineID"));
                F.setFineStatus(rs.getBoolean("FineStatus"));
                F.setFineDate(rs.getDate("FineDate"));
                F.setFineDeadline(rs.getDate("FineDeadline"));
                F.setStudentID(rs.getInt("StudentID"));
                F.setFineTypeID(rs.getInt("FineTypeID"));
                oFine.add(F);
                index++;
            }
            rs.close();

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oFine;
    }

    private static void executeQuery(String query) {

        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Fine> getAllFines() {
        String query = "SELECT * FROM Fine";
        ArrayList<Fine> oFines= getDataByQuery(query);
        return oFines;
    }

    public static ArrayList<Fine> getFineByStudent(String id) {
        String query = "SELECT  * "
                + "FROM Fine "
                + "WHERE StudentID =  " + id;
        ArrayList<Fine> oFine= getDataByQuery(query);
        return oFine;
    }
    
       public static void createFine(Fine fine){
        String query = " INSERT INTO Fine (FineStatus, FineDate, FineDeadline, StudentID, FineTypeID ) " 
                + "VALUES ('"+fine.getFineStatus() +"','"+ fine.getFineDate() +","+fine.getFineDeadline() +","+fine.getStudentID() +","+fine.getFineTypeID()+"')";
        executeQuery(query);
    }

}
