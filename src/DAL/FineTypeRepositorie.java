/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTL.FineType;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author AlbertFields
 */
public class FineTypeRepositorie {
        final static String USER ="DESKTOP-5GUJB6U";
        final static int PORT = 1433;
        final static String DB_NAME ="Comedor";
        
        
  private static ArrayList<FineType> getDataByQuery(String query) {
        ArrayList<FineType> oFineType = new ArrayList<>();
        
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                FineType FT = new FineType();
                FT.setFineTypeID(rs.getInt("FineTypeID"));
                FT.setFineType(rs.getString("FineType"));
                oFineType.add(FT);
                index++;
            }
            rs.close();

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oFineType;
    }

    private static void executeQuery(String query) {

        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<FineType> getAllFineTypes() {
        String query = "SELECT * FROM FineTypes";
        ArrayList<FineType> oFineTypes= getDataByQuery(query);
        return oFineTypes;
    }

    public static ArrayList<FineType> getFineTypeByID(String id) {
        String query = "SELECT  * "
                + "FROM FineTypes "
                + "WHERE FineTypeID =  " + id;
        ArrayList<FineType> oFineType= getDataByQuery(query);
        return oFineType;
    }
    
       public static void createDate(FineType fineType){
        String query = " INSERT INTO FineTypes (FineType) " 
                + "VALUES ('"+fineType.getFineType()+"')";
        executeQuery(query);
    }

}
