/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTL.User;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author AlbertFields
 */
public class UserRepositorie {
        final static String USER ="DESKTOP-5GUJB6U";
        final static int PORT = 1433;
        final static String DB_NAME ="Comedor";
        
        
  private static ArrayList<User> getDataByQuery(String query) {
        ArrayList<User> oUser = new ArrayList<>();
        
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                User U = new User();
                U.setUserID(rs.getInt("UserID"));
                U.setUserName(rs.getString("UserName"));
                U.setPassword(rs.getString("Password"));
                U.setUserTypeID(rs.getInt("UserTypeID"));
                oUser.add(U);
                index++;
            }
            rs.close();

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oUser;
    }

    private static void executeQuery(String query) {

        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public static ArrayList<User> getUserType(String userName){
        String query = "SELECT UserTypeID "
                + "FROM Users "
                + "Where UserName = '" + userName +"'";
        ArrayList<User> oUser = getDataByQuery(query);
        return oUser;
    }
    
    public static ArrayList<User> getUserByLoginData(String password, String userName){
        String query = "SELECT * "
                + "FROM Users "
                + "Where UserName = '" + userName + "' AND Password = '" + password+"'";
        ArrayList<User> oUser = getDataByQuery(query);
        return oUser;
    }

    public static ArrayList<User> getAllUsers() {
        String query = "SELECT * FROM Users";
        ArrayList<User> oUsers= getDataByQuery(query);
        return oUsers;
    }

    public static ArrayList<User> getUserByID(String id) {
        String query = "SELECT  * "
                + "FROM Users "
                + "WHERE UserID =  " + id;
        ArrayList<User> oUser= getDataByQuery(query);
        return oUser;
    }
    
       public static void createFine(User user){
        String query = " INSERT INTO Users (UserName, Password, UserTypeID)" 
                + "VALUES ('"+user.getUserName()+"','"+ user.getPassword() +","+user.getUserTypeID()+"')";
        executeQuery(query);
    }

}
