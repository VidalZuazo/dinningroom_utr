/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTL.Date;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author AlbertFields
 */
public class DateRepositorie {
        final static String USER ="DESKTOP-5GUJB6U";
        final static int PORT = 1433;
        final static String DB_NAME ="Comedor";
        
        
  private static ArrayList<Date> getDataByQuery(String query) {
        ArrayList<Date> oDate = new ArrayList<>();
        
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                Date D = new Date();
                D.setDateID(rs.getInt("DateID"));
                D.setStudentID(rs.getInt("StudentID"));
                D.setVisitDate(rs.getDate("VisitDate"));
                oDate.add(D);
                index++;
            }
            rs.close();

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oDate;
    }

    private static void executeQuery(String query) {

        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Date> getAllDates() {
        String query = "SELECT * FROM Dates";
        ArrayList<Date> oDates= getDataByQuery(query);
        return oDates;
    }

    public static ArrayList<Date> getDatesByStudent(String id) {
        String query = "SELECT  * "
                + "FROM Dates "
                + "WHERE StudentID =  " + id;
        ArrayList<Date> oDate= getDataByQuery(query);
        return oDate;
    }
    
       public static void createDate(Date date){
        String query = " INSERT INTO Dates (StudentID, VisitDate) " 
                + "VALUES ('"+date.getStudentID()+"','"+ date.getVisitDate()+"')";
        executeQuery(query);
    }

}