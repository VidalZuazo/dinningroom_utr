/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTL.UserType;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author AlbertFields
 */
public class UserTypeRepositorie {
        final static String USER ="DESKTOP-5GUJB6U";
        final static int PORT = 1433;
        final static String DB_NAME ="Comedor";
        
        
  private static ArrayList<UserType> getDataByQuery(String query) {
        ArrayList<UserType> oUserType = new ArrayList<>();
        
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                UserType UT = new UserType();
                UT.setUserTypeID(rs.getInt("UserTypeID"));
                UT.setUserName(rs.getString("UserName"));
                oUserType.add(UT);
                index++;
            }
            rs.close();

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oUserType;
    }

    private static void executeQuery(String query) {

        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<UserType> getAllUserTypes() {
        String query = "SELECT * FROM UserTypes";
        ArrayList<UserType> oUserTypes= getDataByQuery(query);
        return oUserTypes;
    }

    public static ArrayList<UserType> getUserTypesByID(String id) {
        String query = "SELECT  * "
                + "FROM UserTypes "
                + "WHERE UserTypeID =  " + id;
        ArrayList<UserType> oUserType= getDataByQuery(query);
        return oUserType;
    }
    
       public static void createDate(UserType userType){
        String query = " INSERT INTO UserTypes (UserName) " 
                + "VALUES ('"+userType.getUserName()+"')";
        executeQuery(query);
    }

}
