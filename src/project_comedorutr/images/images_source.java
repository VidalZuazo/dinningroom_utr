/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project_comedorutr.images;

import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import static project_comedorutr.interfaces.interface_administrator.label_background_adm1;
import static project_comedorutr.interfaces.interface_administrator.label_button_add;
//Log In
import static project_comedorutr.interfaces.interface_login.background_login;
import static project_comedorutr.interfaces.interface_login.label_password;
import static project_comedorutr.interfaces.interface_login.label_user;
//Administrator
import static project_comedorutr.interfaces.interface_administrator.label_menu_admi_1;
import static project_comedorutr.interfaces.interface_administrator_add.label_background_add;
import static project_comedorutr.interfaces.interface_administrator_add.label_logo_administrator_add;
import static project_comedorutr.interfaces.interface_administrator_add.label_menu_admi_add;
import static project_comedorutr.interfaces.interface_administrator_delete.label_background_delete;
import static project_comedorutr.interfaces.interface_administrator_delete.label_logo_administrator_delete;
import static project_comedorutr.interfaces.interface_administrator_delete.label_menu_admi_delete;
import static project_comedorutr.interfaces.interface_administrator_update.label_background_update;
import static project_comedorutr.interfaces.interface_administrator_update.label_logo_administrator_update;
import static project_comedorutr.interfaces.interface_administrator_update.label_menu_admi_update;
import static project_comedorutr.interfaces.interface_cashier.label_background_cashier;
//Cashier
import static project_comedorutr.interfaces.interface_cashier.label_logo_cashier;
import static project_comedorutr.interfaces.interface_cashier.label_menu_cashier;
//Dinningroom
import static project_comedorutr.interfaces.interface_dinningroom.label_background_droom;
import static project_comedorutr.interfaces.interface_dinningroom.label_logo_droom;
import static project_comedorutr.interfaces.interface_dinningroom.label_menu_droom;
import static project_comedorutr.interfaces.interface_administrator.label_button_delete;
import static project_comedorutr.interfaces.interface_administrator.label_button_update;
/**
 *
 * @author Pavilion
 */
public class images_source {

    //Log In Images
    public void background_login() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/comedor_escolar_18-01.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(background_login.getWidth(), background_login.getHeight(), Image.SCALE_DEFAULT));
        background_login.setIcon(i);
    }
    
    public void user_login() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/user.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_user.getWidth(), label_user.getHeight(), Image.SCALE_DEFAULT));
        label_user.setIcon(i);
    }
    
    public void password_login() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/lock.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_password.getWidth(), label_password.getHeight(), Image.SCALE_DEFAULT));
        label_password.setIcon(i);
    }
    
    //Administrator Images
    /*
    public void logo_administrator_main() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_administrator_one.getWidth(), label_logo_administrator_one.getHeight(), Image.SCALE_DEFAULT));
        label_logo_administrator_one.setIcon(i);
    }*/
    
    public void background_administrator_1() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back 3.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_background_adm1.getWidth(), label_background_adm1.getHeight(), Image.SCALE_DEFAULT));
        label_background_adm1.setIcon(i);
    }
    
    public void background_administrator_add() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back 3.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_background_add.getWidth(), label_background_add.getHeight(), Image.SCALE_DEFAULT));
        label_background_add.setIcon(i);
    }
    
    public void background_administrator_update() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back 3.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_background_update.getWidth(), label_background_update.getHeight(), Image.SCALE_DEFAULT));
        label_background_update.setIcon(i);
    }
    
    public void background_administrator_dalete() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back 3.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_background_delete.getWidth(), label_background_delete.getHeight(), Image.SCALE_DEFAULT));
        label_background_delete.setIcon(i);
    }
    
    public void logo_administrator_add() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_administrator_add.getWidth(), label_logo_administrator_add.getHeight(), Image.SCALE_DEFAULT));
        label_logo_administrator_add.setIcon(i);
    }
    
    public void logo_administrator_delete() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_administrator_delete.getWidth(), label_logo_administrator_delete.getHeight(), Image.SCALE_DEFAULT));
        label_logo_administrator_delete.setIcon(i);
    }
    
    public void logo_administrator_update() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_administrator_update.getWidth(), label_logo_administrator_update.getHeight(), Image.SCALE_DEFAULT));
        label_logo_administrator_update.setIcon(i);
    }
    
    public void menu_admi_1() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/menu.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_admi_1.getWidth(), label_menu_admi_1.getHeight(), Image.SCALE_DEFAULT));
        label_menu_admi_1.setIcon(i);
    }
    
    public void menu_back_admi_1() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_admi_1.getWidth(), label_menu_admi_1.getHeight(), Image.SCALE_DEFAULT));
        label_menu_admi_1.setIcon(i);
    }
    
    public void menu_admi_add() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/menu.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_admi_add.getWidth(), label_menu_admi_add.getHeight(), Image.SCALE_DEFAULT));
        label_menu_admi_add.setIcon(i);
    }
    
    public void menu_back_admi_add() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_admi_add.getWidth(), label_menu_admi_add.getHeight(), Image.SCALE_DEFAULT));
        label_menu_admi_add.setIcon(i);
    }
    
    public void menu_admi_delete() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/menu.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_admi_delete.getWidth(), label_menu_admi_delete.getHeight(), Image.SCALE_DEFAULT));
        label_menu_admi_delete.setIcon(i);
    }
    
    public void menu_back_admi_delete() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_admi_delete.getWidth(), label_menu_admi_delete.getHeight(), Image.SCALE_DEFAULT));
        label_menu_admi_delete.setIcon(i);
    }
    
    public void menu_admi_update() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/menu.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_admi_update.getWidth(), label_menu_admi_update.getHeight(), Image.SCALE_DEFAULT));
        label_menu_admi_update.setIcon(i);
    }
    
    public void menu_back_admi_update() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_admi_update.getWidth(), label_menu_admi_update.getHeight(), Image.SCALE_DEFAULT));
        label_menu_admi_update.setIcon(i);
    }
    
    public void button_add_adm1() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/add_big.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_button_add.getWidth(), label_button_add.getHeight(), Image.SCALE_DEFAULT));
        label_button_add.setIcon(i);
    }
    
    public void button_delete_adm1() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/delete_big.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_button_delete.getWidth(), label_button_delete.getHeight(), Image.SCALE_DEFAULT));
        label_button_delete.setIcon(i);
    }
    
    public void button_update_adm1() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/update_big.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_button_update.getWidth(), label_button_update.getHeight(), Image.SCALE_DEFAULT));
        label_button_update.setIcon(i);
    }
    
    //Cashier Images
    
    public void background_cashier() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back 4.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_background_cashier.getWidth(), label_background_cashier.getHeight(), Image.SCALE_DEFAULT));
        label_background_cashier.setIcon(i);
    }
    
    public void logo_cashier() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_cashier.getWidth(), label_logo_cashier.getHeight(), Image.SCALE_DEFAULT));
        label_logo_cashier.setIcon(i);
    }
    
    public void menu_cashier() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/menu.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_cashier.getWidth(), label_menu_cashier.getHeight(), Image.SCALE_DEFAULT));
        label_menu_cashier.setIcon(i);
    }
    
    public void menu_back_cashier() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_cashier.getWidth(), label_menu_cashier.getHeight(), Image.SCALE_DEFAULT));
        label_menu_cashier.setIcon(i);
    }
    
    //Dinningroom Images
    
    public void background_droom() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back 2.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_background_droom.getWidth(), label_background_droom.getHeight(), Image.SCALE_DEFAULT));
        label_background_droom.setIcon(i);
    }
    
    public void logo_dinningroom() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_droom.getWidth(), label_logo_droom.getHeight(), Image.SCALE_DEFAULT));
        label_logo_droom.setIcon(i);
    }
    
    public void menu_dinningroom() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/menu.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_droom.getWidth(), label_menu_droom.getHeight(), Image.SCALE_DEFAULT));
        label_menu_droom.setIcon(i);
    }
    
    public void menu_back_dinningroom() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/back.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_menu_droom.getWidth(), label_menu_droom.getHeight(), Image.SCALE_DEFAULT));
        label_menu_droom.setIcon(i);
    }

}
