/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL;

/**
 *
 * @author AlbertFields
 */
public class Student {

    private int StudentID;
    private String firstName;
    private String lastName;
    private String major;
    private String Email;
    private boolean studentStatus;
    private String Birthdate;
    private String Curp;
    private int Height;
    private int SWeight;
    private String City;
    private String Adress;

    public Student() {
    }

    public Student(int StudentID, String firstName, String lastName, String major, String Email, boolean studentStatus, String Birthdate, String Curp, int Height, int SWeight, String City, String Adress) {
        this.StudentID = StudentID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.major = major;
        this.Email = Email;
        this.studentStatus = studentStatus;
        this.Birthdate = Birthdate;
        this.Curp = Curp;
        this.Height = Height;
        this.SWeight = SWeight;
        this.City = City;
        this.Adress = Adress;
    }

    public Student(String firstName, String lastName, String major, String Email, boolean studentStatus, String Birthdate, String Curp, int Height, int SWeight, String City, String Adress) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.major = major;
        this.Email = Email;
        this.studentStatus = studentStatus;
        this.Birthdate = Birthdate;
        this.Curp = Curp;
        this.Height = Height;
        this.SWeight = SWeight;
        this.City = City;
        this.Adress = Adress;
    }

    

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int StudentID) {
        this.StudentID = StudentID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public boolean getStudentStatus() {
        return studentStatus;
    }

    public void setStudentStatus(boolean studentStatus) {
        this.studentStatus = studentStatus;
    }

    public String getBirthdate() {
        return Birthdate;
    }

    public void setBirthdate(String Birthdate) {
        this.Birthdate = Birthdate;
    }

    public String getCurp() {
        return Curp;
    }

    public void setCurp(String Curp) {
        this.Curp = Curp;
    }

    public int getHeight() {
        return Height;
    }

    public void setHeight(int Height) {
        this.Height = Height;
    }

    public int getSWeight() {
        return SWeight;
    }

    public void setSWeight(int SWeight) {
        this.SWeight = SWeight;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getAdress() {
        return Adress;
    }

    public void setAdress(String Adress) {
        this.Adress = Adress;
    }
    
    

 
}
