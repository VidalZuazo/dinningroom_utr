/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL;


/**
 *
 * @author AlbertFields
 */
public class Date {
    
    private int dateID;
    private int studentID;
    private java.util.Date visitDate;
    
    public Date(){
        
    }
    
    public Date(int _dateID, int _studentID, java.util.Date _vistDate){
        this.dateID = _dateID;
        this.studentID = _studentID;
        this.visitDate = _vistDate;           
    }

    public int getDateID() {
        return dateID;
    }

    public void setDateID(int dateID) {
        this.dateID = dateID;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public java.util.Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(java.util.Date visitDate) {
        this.visitDate = visitDate;
    }
       
    
}
