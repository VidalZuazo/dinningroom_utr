/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL;

/**
 *
 * @author AlbertFields
 */
public class FineType {
    private int fineTypeID;
    private String fineType;

    public FineType() {
    }

    public FineType(int _fineTypeID, String _fineType) {
        this.fineTypeID = _fineTypeID;
        this.fineType = _fineType;
    }

    public int getFineTypeID() {
        return fineTypeID;
    }

    public void setFineTypeID(int fineTypeID) {
        this.fineTypeID = fineTypeID;
    }

    public String getFineType() {
        return fineType;
    }

    public void setFineType(String fineType) {
        this.fineType = fineType;
    }

}
