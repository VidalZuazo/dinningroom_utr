/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL;

/**
 *
 * @author AlbertFields
 */
public class User {
    private int userID;
    private String userName;
    private String password;
    private int userTypeID;

    public User() {
    }

    public User(int _userID, String _userName, String _password, int _userTypeID) {
        this.userID = _userID;
        this.userName =_userName;
        this.password =_password;
        this.userTypeID =_userTypeID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(int userTypeID) {
        this.userTypeID = userTypeID;
    }
    
    
    
    
}
